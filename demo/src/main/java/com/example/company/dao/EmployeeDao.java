package com.example.company.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.company.data.model.Employee;

@Repository
public interface EmployeeDao extends CrudRepository<Employee, Integer> {}
