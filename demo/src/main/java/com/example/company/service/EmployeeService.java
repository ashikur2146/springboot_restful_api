package com.example.company.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.company.dao.EmployeeDao;
import com.example.company.data.model.Employee;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeDao employeeDao;
	
	public List<? extends Object> getAllEmployees() {
		return StreamSupport.stream(employeeDao.findAll().spliterator(), false).collect(Collectors.toList());
	}
	
	public void addEmployee(Employee employee) {
		employeeDao.save(employee);
	}
	
	public void updateEmployee(Employee employee) {
		employeeDao.save(employee);
	}
	
	public void deleteEmployee(Employee employee) {
		employeeDao.delete(employee);
	}
	
	public void deleteEmployee(Integer employeeId) {
		employeeDao.deleteById(employeeId);
	}
	
	public Employee findEmployee(Integer id) {
		return employeeDao.findById(id).orElse(null);
	}
}
