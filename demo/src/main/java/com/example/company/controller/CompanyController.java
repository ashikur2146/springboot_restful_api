package com.example.company.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.company.data.model.Employee;
import com.example.company.service.EmployeeService;

@RestController
@RequestMapping(value="company")
public class CompanyController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value="/getAllEmployees", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<?> getAllEmployees(final HttpServletRequest httpServletRequest) {
		return ResponseEntity.ok(employeeService.getAllEmployees());
	}
	
	@RequestMapping(value="/addEmployee", method=RequestMethod.POST, consumes="application/json", produces="application/json")
	@ResponseBody
	public ResponseEntity<?> addEmployee(final HttpServletRequest httpServletRequest, final @RequestBody Employee employee) {
		employeeService.addEmployee(employee);
		return ResponseEntity.ok(HttpStatus.OK);
	}
	
	@RequestMapping(value="/updateEmployee", method=RequestMethod.PUT, consumes="application/json", produces="application/json")
	@ResponseBody
	public ResponseEntity<?> updateEmployee(final HttpServletRequest httpServletRequest, final @RequestBody Employee employee) {
		employeeService.updateEmployee(employee);
		return ResponseEntity.ok(HttpStatus.OK);
	}
	
	@RequestMapping(value="/deleteEmployee", method=RequestMethod.DELETE, consumes="application/json", produces="application/json")
	@ResponseBody
	public ResponseEntity<?> deleteEmployee(final HttpServletRequest httpServletRequest, final @RequestBody Employee employee) {
		employeeService.deleteEmployee(employee);
		return ResponseEntity.ok(HttpStatus.OK);
	}
	
	@RequestMapping(value="/findEmployee", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<?> findEmployee(final HttpServletRequest httpServletRequest, final @RequestParam Integer id) {
		return ResponseEntity.ok(employeeService.findEmployee(id));
	}
}
